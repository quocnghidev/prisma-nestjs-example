## RestAPI Prisma, Postgres, NestJS Example

### Install dependencies

```
npm i
```

### Run postgres with Docker

```
docker-compose up -d
```

### Create sample data

```
npx prisma migrate dev
```

### Add seeding data

```
npx prisma db seed
```

### Run server

```
npm run start:dev
```

### Test API

```
http://localhost:3000/api/
```
